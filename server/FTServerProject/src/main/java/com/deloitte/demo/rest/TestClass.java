package com.deloitte.demo.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.deloitte.demo.core.UserModel;

@Component
@Path("/test")
public class TestClass {

	@GET 
	@Path("getusermodel")
	@Consumes({MediaType.TEXT_PLAIN})
	@Produces({MediaType.APPLICATION_JSON})	
	//@Transactional
	public UserModel getUserModel(String x) {
		
		UserModel model = new UserModel();
		
		model.setUserId(1);
		model.setFirstName("Mukesh");
		model.setLastName("Patel");
		
		return model; 		
	}
	
}

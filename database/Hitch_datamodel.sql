
create schema ftdschema;

set search_path to ftdschema;

CREATE TABLE ftdschema.app_user (
                user_id BIGINT NOT NULL,
                name VARCHAR(64),
                is_user_verified BOOLEAN,
                pin SMALLINT NOT NULL,
                gcm_reg_id VARCHAR NOT NULL,
                password VARCHAR(15) NOT NULL,
                contact_number BIGINT NOT NULL,
                authentication_token VARCHAR(128),
                email_id VARCHAR(64) NOT NULL,
				user_prof_pic_path VARCHAR(255) NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_by VARCHAR NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                CONSTRAINT app_user_pk PRIMARY KEY (user_id)
);
COMMENT ON TABLE ftdschema.app_user IS 'Stores Deloitte''s user details';
COMMENT ON COLUMN ftdschema.app_user.user_id IS 'stores user id';
COMMENT ON COLUMN ftdschema.app_user.first_name IS 'stores first name of user';
COMMENT ON COLUMN ftdschema.app_user.last_name IS 'stores last name of user';
COMMENT ON COLUMN ftdschema.app_user.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.app_user.is_user_verified IS 'stores flag to check if user is a verified user';
COMMENT ON COLUMN ftdschema.app_user.pin IS 'stores user''s pin for verification';
COMMENT ON COLUMN ftdschema.app_user.updated_by IS 'latest updator of this record';
COMMENT ON COLUMN ftdschema.app_user.created_ts IS 'created timestamp';
COMMENT ON COLUMN ftdschema.app_user.updated_ts IS 'updated timestamp';
COMMENT ON COLUMN ftdschema.app_user.gcm_reg_id IS 'stores gcm registeration id';
COMMENT ON COLUMN ftdschema.app_user.password IS 'stores account password of user';
COMMENT ON COLUMN ftdschema.app_user.contact_number IS 'stores mobile no. of user';
COMMENT ON COLUMN ftdschema.app_user.authentication_token IS 'stores auth token for user to access google api';
COMMENT ON COLUMN ftdschema.app_user.email_id IS 'stores email id of user';


CREATE TABLE ftdschema.seeker (
                seeker_id BIGINT NOT NULL,
                seeker_user_id BIGINT NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                updated_by VARCHAR NOT NULL,
                seeker_current_latitude DOUBLE PRECISION NOT NULL,
                seeker_current_longitude DOUBLE PRECISION NOT NULL,
                CONSTRAINT seeker_pk PRIMARY KEY (seeker_id)
);
COMMENT ON COLUMN ftdschema.seeker.created_ts IS 'created timestamp';
COMMENT ON COLUMN ftdschema.seeker.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.seeker.updated_ts IS 'updated timestamp';
COMMENT ON COLUMN ftdschema.seeker.updated_by IS 'latest updator of this record';


CREATE TABLE ftdschema.vehicle (
                vehicle_id BIGINT NOT NULL,
                user_id BIGINT NOT NULL,
                seating_capacity SMALLINT NOT NULL,
                updated_by VARCHAR NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                color VARCHAR(15) NOT NULL,
                vehical_number VARCHAR(32) NOT NULL,
                model_name VARCHAR(32) NOT NULL,
                CONSTRAINT vehicle_pk PRIMARY KEY (vehicle_id)
);
COMMENT ON TABLE ftdschema.vehicle IS 'Stores the vehicle details';
COMMENT ON COLUMN ftdschema.vehicle.user_id IS 'stores user id';
COMMENT ON COLUMN ftdschema.vehicle.updated_by IS 'latest updator of this record';
COMMENT ON COLUMN ftdschema.vehicle.created_ts IS 'created timestamp';
COMMENT ON COLUMN ftdschema.vehicle.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.vehicle.updated_ts IS 'updated timestamp';


CREATE TABLE ftdschema.journey (
                journey_id BIGINT NOT NULL,
                vehicle_id BIGINT NOT NULL,
                provider_user_id BIGINT NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_by VARCHAR NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                end_loc_latitude DOUBLE PRECISION NOT NULL,
                end_loc_longitude DOUBLE PRECISION NOT NULL,
                start_loc_latitude DOUBLE PRECISION NOT NULL,
                prov_current_longitude DOUBLE PRECISION NOT NULL,
                prov_current_latitude DOUBLE PRECISION NOT NULL,
                journey_start_time TIMESTAMP NOT NULL,
                journey_end_time TIMESTAMP NOT NULL,
                is_journey_completed BOOLEAN NOT NULL,
                start_loc_longitude DOUBLE PRECISION NOT NULL,
                CONSTRAINT journey_pk PRIMARY KEY (journey_id)
);
COMMENT ON TABLE ftdschema.journey IS 'stores journey details';
COMMENT ON COLUMN ftdschema.journey.updated_ts IS 'updated timestamp';
COMMENT ON COLUMN ftdschema.journey.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.journey.updated_by IS 'latest updator of this record';
COMMENT ON COLUMN ftdschema.journey.created_ts IS 'created timestamp';


CREATE TABLE ftdschema.seeker_journey (
                seeker_journey_id BIGINT NOT NULL,
                journey_id BIGINT NOT NULL,
                seeker_id BIGINT NOT NULL,
                seeker_pickup_lat DOUBLE PRECISION NOT NULL,
                seeker_pickup_long DOUBLE PRECISION NOT NULL,
                seeker_drop_time TIMESTAMP NOT NULL,
                seeker_drop_long DOUBLE PRECISION NOT NULL,
                seeker_pickup_time TIMESTAMP NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_by VARCHAR NOT NULL,
                seeker_drop_lat DOUBLE PRECISION NOT NULL,
                CONSTRAINT seeker_journey_pk PRIMARY KEY (seeker_journey_id)
);
COMMENT ON COLUMN ftdschema.seeker_journey.updated_ts IS 'updated timestamp';
COMMENT ON COLUMN ftdschema.seeker_journey.created_ts IS 'created timestamp';
COMMENT ON COLUMN ftdschema.seeker_journey.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.seeker_journey.updated_by IS 'latest updator of this record';


CREATE TABLE ftdschema.points (
                points_id BIGINT NOT NULL,
                journey_id BIGINT NOT NULL,
                user_id BIGINT NOT NULL,
                number_of_points INTEGER NOT NULL,
                description VARCHAR(200) NOT NULL,
                service_type_sw VARCHAR(1) NOT NULL,
                updated_ts TIMESTAMP NOT NULL,
                created_ts TIMESTAMP NOT NULL,
                created_by VARCHAR NOT NULL,
                updated_by VARCHAR NOT NULL,
                CONSTRAINT points_pk PRIMARY KEY (points_id)
);
COMMENT ON TABLE ftdschema.points IS 'Stores points details of a user';
COMMENT ON COLUMN ftdschema.points.service_type_sw IS 'Will hold - S:Seeker, P:Provider';
COMMENT ON COLUMN ftdschema.points.updated_ts IS 'updated timestamp';
COMMENT ON COLUMN ftdschema.points.created_ts IS 'created timestamp';
COMMENT ON COLUMN ftdschema.points.created_by IS 'record creator';
COMMENT ON COLUMN ftdschema.points.updated_by IS 'latest updator of this record';


ALTER TABLE ftdschema.journey ADD CONSTRAINT user_journey_fk
FOREIGN KEY (provider_user_id)
REFERENCES ftdschema.app_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.vehicle ADD CONSTRAINT user_vehicle_fk
FOREIGN KEY (user_id)
REFERENCES ftdschema.app_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.points ADD CONSTRAINT user_points_fk
FOREIGN KEY (user_id)
REFERENCES ftdschema.app_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.seeker ADD CONSTRAINT user_seeker_fk
FOREIGN KEY (seeker_user_id)
REFERENCES ftdschema.app_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.seeker_journey ADD CONSTRAINT seeker_seeker_journey_fk
FOREIGN KEY (seeker_id)
REFERENCES ftdschema.seeker (seeker_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.journey ADD CONSTRAINT vehicle_journey_fk
FOREIGN KEY (vehicle_id)
REFERENCES ftdschema.vehicle (vehicle_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.points ADD CONSTRAINT journey_points_fk
FOREIGN KEY (journey_id)
REFERENCES ftdschema.journey (journey_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE ftdschema.seeker_journey ADD CONSTRAINT journey_seeker_journey_fk
FOREIGN KEY (journey_id)
REFERENCES ftdschema.journey (journey_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
